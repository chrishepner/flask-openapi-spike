#!/bin/bash

poetry run datamodel-codegen \
  --input openapi.yaml \
  --input-file-type openapi \
  --target-python-version 3.10  \
  --openapi-scopes schemas paths \
  --output model.py
