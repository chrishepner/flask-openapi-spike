"""The flask application/entry point."""

from flask import Flask
from flask_pydantic import validate
from pydantic import BaseModel, ValidationError, validator

import model

app = Flask(__name__)

class CoolPet(model.Pet):
    """
    Generated Pet model, with our own custom validation
    """
    @validator('name')
    def name_should_be_cool(cls, v):
        if 'cool' not in v:
            raise ValueError("Name must be cool")
        return v


class PetGetPathParameters(BaseModel):
    """datamodel-codegen doesn't support generating this yet.
    We'd have to define this ourselves for now.
    https://github.com/koxudaxi/datamodel-code-generator/issues/1071
    """
    pet_id: int


class Standard404(BaseModel):
    message: str


STANDARD_404_RESPONSE = (Standard404(message="Not Found"), 404)


def _fetch_pet(pet_id: int):
    # ids 2, 4 will fail the validator we defined on CoolPet
    dog_label = 'cool' if pet_id % 2 == 1 else 'fool'
    name = f"a {dog_label} dog"
    print(name)

    return CoolPet(
        id=pet_id,
        name=name,
        category=model.Category(
            id=1,
            name='Big Dogz'
        ),
        photoUrls=[],
        tags=None,
        status=model.Status1.available,
    )

@app.route("/")
def hello_world():
    return "<p>Hi! see /pet/1!</p>"


@app.route("/pet/<pet_id>", methods=['GET'])
@validate()
def get_pet(**kwargs):
    try:
        # Validate path param input
        params = PetGetPathParameters(**kwargs)
    except ValidationError as e:
        return e.json(), 400

    if params.pet_id > 5:
        return STANDARD_404_RESPONSE

    try:
        return _fetch_pet(params.pet_id)
    except ValidationError as e:
        # probably wouldn't actually want to return this!
        # but for illustrative purposes:
        return e.json(), 500
